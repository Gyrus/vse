<?php

namespace AppBundle\DataFixtures;


use AppBundle\Entity\Region;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class RegionFixture extends Fixture
{
    private const REGIONSNAME = [
        'Санкт-Петербург',
        'Уфа',
        'Нижний Новгород',
        'Владимир',
        'Кострома',
        'Екатеринбург',
        'Ковров',
        'Воронеж',
        'Самара',
        'Астрахань'
    ];

    public function load(ObjectManager $manager)
    {
        /** @var Generator */
        $faker = Factory::create();
        foreach (self::REGIONSNAME as $regionName)
        {
            $region = new Region();
            $region->setRegionName($regionName);
            $region->setPathDays($faker->numberBetween(3, 20));
            $manager->persist($region);
        }
        $manager->flush();
    }
}