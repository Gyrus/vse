<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Courier;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class CourierFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /** @var Generator */
        $faker = Factory::create();
        for ($i = 0; $i <= 10; $i++){
            $courier = new Courier();
            $courier->setCourierName($faker->firstName);
            $courier->setCourierSurname($faker->lastName);
            $manager->persist($courier);
        }
        $manager->flush();
    }
}