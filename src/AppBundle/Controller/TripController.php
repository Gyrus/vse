<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Trip;
use AppBundle\Form\TripType;
use AppBundle\Service\TripService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TripController extends Controller
{
    /** @var TripService */
    private $tripService;

    /**
     * TripController constructor.
     * @param TripService $tripService
     */
    public function __construct(TripService $tripService)
    {
        $this->tripService = $tripService;
    }

    /**
     * @Route("/addTrip")
     */
    public function addTripAction(Request $request)
    {
        $trip = new Trip();
        $form = $this->createForm(TripType::class, $trip);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);
            $em->flush();
            return $this->redirect(
                $this->generateUrl('show_trip', array(), UrlGeneratorInterface::ABSOLUTE_URL)
            );
        }
        return $this->render('@App/Trip/add_trip.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/showTrip", name="show_trip")
     * @throws \Exception
     */
    public function showTripAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tripRepository = $em->getRepository('AppBundle:Trip');
        $dayTime = new \DateTime('now');
        $dayTime->setTime(00,00,00);
        /** @var Trip $todayTrips */
        $todayTrips = $tripRepository->findTripsForDay($dayTime);
        return $this->render('@App/Trip/show_trip.html.twig', array(
            'todayTrips' => $todayTrips
        ));
    }

}
