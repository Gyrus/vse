<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Trip;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Tests\Fixtures\includes\HotPath\P1;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller
{
    /**
     * @Route("/showTimeInTrip")
     * @Method("POST")
     */
    public function showTimeInTripAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $regionRepository = $em->getRepository('AppBundle:Region');
            $region = $regionRepository->find($request->request->get('appbundle_trip')['region']);
            return new JsonResponse(['daysInTrip' => $region->getPathDays()]);
        } else {
            return new Response('Only ajax call', 400);
        }

    }

    /**
     * @Route("/checkCourier")
     * @Method("POST")
     */
    public function checkCourierIsAvailable(Request $request)
    {
        if($request->isXmlHttpRequest()){
            $em = $this->getDoctrine()->getManager();
            $tripRepository = $em->getRepository('AppBundle:Trip');
            $dayTime = new \DateTime('now');
            $dayTime->setTime(00,00,00);
            /** @var Trip $todayTrips */
            $todayTrips = $tripRepository->findTripsForDay($dayTime);
            foreach ($todayTrips as $key=>$todayTrip) {
                $couriersId[$key] = $todayTrip->getCourier()->getId();
            }
            return new JsonResponse($couriersId);
        }else{
            return new Response("Only ajax call", 400);
        }
    }
}
