<?php

namespace AppBundle\Service;


class TripService
{
    /**
     * @param int $days
     * @return \DateTime
     * @throws \Exception
     */
    public function transferIntIntoDate(int $days):\DateTime
    {
        $currentDay = new \DateTime('now');
        return $currentDay->modify('+ '.$days.' days');
    }
}