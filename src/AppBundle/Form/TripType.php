<?php

namespace AppBundle\Form;

use AppBundle\Entity\Courier;
use AppBundle\Entity\Region;
use AppBundle\Entity\Trip;
use AppBundle\Service\TripService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TripType extends AbstractType
{
    /** @var TripService */
    private $TripService;

    /**
     * TripType constructor.
     * @param TripService $TripService
     */
    public function __construct(TripService $TripService)
    {
        $this->TripService = $TripService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('region', EntityType::class, array(
                'class' => Region::class,
                'choice_label' => 'regionName',
            ))
            ->add('courier', EntityType::class, array(
                'class' => Courier::class,
                'choice_label' => 'courierName'
            ))
            ->add('submit', SubmitType::class)
            ->addEventListener(FormEvents::SUBMIT, array($this, 'onSubmitData'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Trip'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_trip';
    }

    /**
     * @param FormEvent $event
     * @throws \Exception
     */
    public function onSubmitData(FormEvent $event)
    {
        /** @var Trip $data */
        $data = $event->getData();
        $data->setDaysInTrip($this->TripService->transferIntIntoDate(
            $event->getForm()->get('region')->getNormData()->getPathDays()
        ));
        $event->setData($data);
    }
}
