<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trip
 *
 * @ORM\Table(name="trip")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TripRepository")
 */
class Trip
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="daysInTrip", type="datetime")
     */
    private $daysInTrip;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Region", inversedBy="trip")
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Courier", inversedBy="trip")
     */
    private $courier;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDaysInTrip(): \DateTime
    {
        return $this->daysInTrip;
    }

    /**
     * @param \DateTime $daysInTrip
     */
    public function setDaysInTrip(\DateTime $daysInTrip): void
    {
        $this->daysInTrip = $daysInTrip;
    }

    /**
     * Set region.
     *
     * @param \AppBundle\Entity\Region|null $region
     *
     * @return Trip
     */
    public function setRegion(\AppBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \AppBundle\Entity\Region|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set courier.
     *
     * @param \AppBundle\Entity\Courier|null $courier
     *
     * @return Trip
     */
    public function setCourier(\AppBundle\Entity\Courier $courier = null)
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * Get courier.
     *
     * @return \AppBundle\Entity\Courier|null
     */
    public function getCourier()
    {
        return $this->courier;
    }
}
