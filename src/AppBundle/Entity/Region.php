<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegionRepository")
 */
class Region
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="regionName", type="string", length=100, unique=true)
     */
    private $regionName;

    /**
     * @var int
     *
     * @ORM\Column(name="pathDays", type="integer")
     */
    private $pathDays;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Trip", mappedBy="region")
     */
    private $trip;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set regionName.
     *
     * @param string $regionName
     *
     * @return Region
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;

        return $this;
    }

    /**
     * Get regionName.
     *
     * @return string
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * Set pathDays.
     *
     * @param int $pathDays
     *
     * @return Region
     */
    public function setPathDays($pathDays)
    {
        $this->pathDays = $pathDays;

        return $this;
    }

    /**
     * Get pathDays.
     *
     * @return int
     */
    public function getPathDays()
    {
        return $this->pathDays;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trip = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add trip.
     *
     * @param \AppBundle\Entity\Trip $trip
     *
     * @return Region
     */
    public function addTrip(\AppBundle\Entity\Trip $trip)
    {
        $this->trip[] = $trip;

        return $this;
    }

    /**
     * Remove trip.
     *
     * @param \AppBundle\Entity\Trip $trip
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrip(\AppBundle\Entity\Trip $trip)
    {
        return $this->trip->removeElement($trip);
    }

    /**
     * Get trip.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrip()
    {
        return $this->trip;
    }
}
