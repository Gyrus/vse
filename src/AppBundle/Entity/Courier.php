<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Courier
 *
 * @ORM\Table(name="courier")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CourierRepository")
 */
class Courier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="courierName", type="string", length=50)
     */
    private $courierName;

    /**
     * @var string
     *
     * @ORM\Column(name="courierPatronymic", type="string", length=50, nullable=true)
     */
    private $courierPatronymic;

    /**
     * @var string
     *
     * @ORM\Column(name="courierSurname", type="string", length=50)
     */
    private $courierSurname;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Trip", mappedBy="courier")
     */
    private $trip;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courierName
     *
     * @param string $courierName
     *
     * @return Courier
     */
    public function setCourierName($courierName)
    {
        $this->courierName = $courierName;

        return $this;
    }

    /**
     * Get courierName
     *
     * @return string
     */
    public function getCourierName()
    {
        return $this->courierName;
    }

    /**
     * Set courierPatronymic
     *
     * @param string $courierPatronymic
     *
     * @return Courier
     */
    public function setCourierPatronymic($courierPatronymic)
    {
        $this->courierPatronymic = $courierPatronymic;

        return $this;
    }

    /**
     * Get courierPatronymic
     *
     * @return string
     */
    public function getCourierPatronymic()
    {
        return $this->courierPatronymic;
    }

    /**
     * Set courierSurname
     *
     * @param string $courierSurname
     *
     * @return Courier
     */
    public function setCourierSurname($courierSurname)
    {
        $this->courierSurname = $courierSurname;

        return $this;
    }

    /**
     * Get courierSurname
     *
     * @return string
     */
    public function getCourierSurname()
    {
        return $this->courierSurname;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trip = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add trip.
     *
     * @param \AppBundle\Entity\Trip $trip
     *
     * @return Courier
     */
    public function addTrip(\AppBundle\Entity\Trip $trip)
    {
        $this->trip[] = $trip;

        return $this;
    }

    /**
     * Remove trip.
     *
     * @param \AppBundle\Entity\Trip $trip
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrip(\AppBundle\Entity\Trip $trip)
    {
        return $this->trip->removeElement($trip);
    }

    /**
     * Get trip.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrip()
    {
        return $this->trip;
    }

    /**
     * Return full courier name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->getCourierName(). ' ' . $this->getCourierPatronymic(). ' ' .$this->getCourierSurname();
    }
}
