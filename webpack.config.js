var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .enableSassLoader()
    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
;

// экспортировать финальную конфигурацию
module.exports = Encore.getWebpackConfig();