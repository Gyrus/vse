.checkout
=========

To install project yuo need to do next commands:
1. composer install
2. bin/console doc:data:create
3. bin/console doc:mig:migrate
4. bin/console doc:fix:load
5. yarn install
6. yarn encore dev